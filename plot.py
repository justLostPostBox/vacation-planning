import matplotlib.pyplot as plot
import matplotlib.ticker as ticker
import numpy as np

from scipy.optimize import curve_fit

vacation_period = [  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14 ]
nb_sights =       [ 14, 24, 26, 39, 49, 51, 61, 64, 72, 76, 81, 87, 92, 96 ]

# scatter plot
plot.scatter(vacation_period, nb_sights)
plot.xlabel('vacation days')
plot.ylabel('sights')
plot.gca().xaxis.set_major_locator(ticker.MultipleLocator(1))
plot.locator_params(axis='y', nbins=10)
plot.show()

# least square method
def straight(x, k, b):
    return k*x + b

fitting_parameters, covariance = curve_fit(straight, vacation_period, nb_sights)
plot.scatter(vacation_period, nb_sights)
plot.xlabel('vacation days')
plot.ylabel('sights')
plot.gca().xaxis.set_major_locator(ticker.MultipleLocator(1))
plot.locator_params(axis='y', nbins=10)
x_fit = np.linspace(min(vacation_period), max(vacation_period))
plot.plot(x_fit, straight(x_fit, *fitting_parameters))
plot.show()

print(fitting_parameters)